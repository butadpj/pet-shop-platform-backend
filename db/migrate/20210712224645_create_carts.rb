class CreateCarts < ActiveRecord::Migration[6.1]
  def change
    create_table :carts do |t|
      t.belongs_to :customer, null: false, foreign_key: true
      t.decimal :total_cart_price, precision: 10, scale: 2
      t.integer :total_cart_items
      t.timestamps
    end
  end
end
