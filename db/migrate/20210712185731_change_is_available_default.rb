class ChangeIsAvailableDefault < ActiveRecord::Migration[6.1]
  def change
    change_column_default :pet_items, :is_available, from: true, to: nil
    # Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
