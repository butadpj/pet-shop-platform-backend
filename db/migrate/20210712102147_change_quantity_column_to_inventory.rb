class ChangeQuantityColumnToInventory < ActiveRecord::Migration[6.1]
  def change
    rename_column :pet_items, :quantity, :inventory
    #Ex:- rename_column("admin_users", "pasword","hashed_pasword")
  end
end
