class CreateCartItems < ActiveRecord::Migration[6.1]
  def change
    create_table :cart_items do |t|
      t.belongs_to :cart, null: false, foreign_key: true
      t.belongs_to :pet_item, null: false, foreign_key: true
      t.integer :quantity
      t.decimal :total_price, precision: 10, scale: 2

      t.timestamps
    end
  end
end
