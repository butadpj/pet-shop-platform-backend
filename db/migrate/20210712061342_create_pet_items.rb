class CreatePetItems < ActiveRecord::Migration[6.1]
  def change
    create_table :pet_items do |t|
      t.string :name
      t.string :description
      t.decimal :price, precision: 10, scale: 2
      t.integer :quantity

      t.timestamps
    end
  end
end
