class AddIsAvailableColumn < ActiveRecord::Migration[6.1]
  def change
    add_column :pet_items, :is_available, :boolean, default: true
    # Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end
