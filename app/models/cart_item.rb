class CartItem < ApplicationRecord
  belongs_to :cart
  belongs_to :pet_item

  has_many :products
end
