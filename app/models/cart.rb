class Cart < ApplicationRecord
  belongs_to :customer

  validates :customer_id, presence: true, uniqueness: true

  has_many :cart_items, dependent: :destroy
end
