class PetItem < ApplicationRecord
  validates :name, presence: true
  validates :price, presence: true

  has_many :cart_items, dependent: :destroy

  after_create :set_is_available_default

  def set_is_available_default
    @pet_item = PetItem.last
    if @pet_item.is_available.nil?
      @pet_item.is_available = true
      @pet_item.save
    end
  end
end
