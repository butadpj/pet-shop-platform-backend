module Types
  class CartItemType < Types::BaseObject
    field :id, ID, null: false
    field :cart_id, Integer, null: false
    field :cart, Types::CartType, null: false
    field :pet_item_id, Integer, null: false
    field :pet_items, [Types::PetItemType], null: false
    field :quantity, Integer, null: false

    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
