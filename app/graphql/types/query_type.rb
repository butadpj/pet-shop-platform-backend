module Types
  class QueryType < Types::BaseObject
    field :users, [Types::UserType], null: true do
      description 'Query all Users'
    end

    field :customers, [Types::CustomerType], null: true do
      description 'Query all Customers'
    end

    field :pet_items, [Types::PetItemType], null: true do
      description 'Query all Pet Items'
    end

    field :carts, [Types::CartType], null: true do
      description 'Query all Carts'
    end

    field :total_cart_items, Integer, null: true do
      description 'Query the total number of user\'s cart items'
    end

    field :cart_items, [Types::CartItemType], null: true do
      description 'Query all Cart Items'
    end

    field :total_pet_items, Integer, null: true do
      description 'Query the total number of Pet Items'
    end

    field :current_user, Types::UserType, null: true do
      description 'Query the currently logged in User'
    end

    def users
      User.all
    end

    def customers
      Customer.all
    end

    def pet_items
      PetItem.all.order('updated_at DESC')
    end

    def carts
      Cart.all.order('updated_at DESC')
    end

    def cart_items
      CartItem.all.order('updated_at DESC')
    end

    def current_user
      context[:current_user]
    end

    def total_pet_items
      PetItem.all.count
    end

    def total_cart_items
      if context[:current_user]
        @cart = context[:current_user].customer.cart
        @total_cart_items = @cart.cart_items.sum(:quantity)
      else
        @total_cart_items = 0
      end
      @total_cart_items
    end
  end
end
