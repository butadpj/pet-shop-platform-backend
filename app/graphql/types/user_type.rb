module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :first_name, String, null: true
    field :last_name, String, null: true
    field :username, String, null: true
    field :password_digest, String, null: true
    field :is_admin, Boolean, null: true

    field :customer, Types::CustomerType, null: true, method: :customer

    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
