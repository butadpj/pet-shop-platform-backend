module Types
  class CartType < Types::BaseObject
    field :id, ID, null: false

    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
    field :total_cart_price, Float, null: true
    field :total_cart_items, Integer, null: true

    field :cart_items, [Types::CartItemType], null: true
    field :from_customer, Types::CustomerType, null: false, method: :customer
  end
end
