module Types
  class CustomerType < Types::BaseObject
    field :id, ID, null: false
    field :user_id, Integer, null: false

    field :user, Types::UserType, null: false
    field :cart, Types::CartType, null: false

    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
