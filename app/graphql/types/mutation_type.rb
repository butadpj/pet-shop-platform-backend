module Types
  class MutationType < Types::BaseObject
    field :add_cart_item, mutation: Mutations::AddCartItem
    field :update_pet_item, mutation: Mutations::UpdatePetItem
    field :delete_pet_item, mutation: Mutations::DeletePetItem
    field :create_pet_item, mutation: Mutations::CreatePetItem
    field :logout_user, mutation: Mutations::LogoutUser
    field :login_user, mutation: Mutations::LoginUser
    field :register_user, mutation: Mutations::RegisterUser
  end
end
