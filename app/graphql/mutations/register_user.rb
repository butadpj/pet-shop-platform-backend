module Mutations
  class RegisterUser < BaseMutation
    field :user, Types::UserType, null: false
    field :message, String, null: true

    argument :first_name, String, required: true
    argument :last_name, String, required: true
    argument :credentials, Types::AuthProviderCredentialsInput, required: true

    def resolve(first_name:, last_name:, credentials:)
      @user = User.create!(
        first_name: first_name,
        last_name: last_name,
        username: credentials&.[](:username),
        password: credentials&.[](:password)
      )

      @customer = Customer.create!(
        user_id: @user.id
      )

      @cart = Cart.create!(customer_id: @customer.id, total_cart_items: 0, total_cart_price: 0)

      @message = 'Successfuly created a new user/customer with cart!'

      @token = AuthToken.token_for_user(@user)

      context[:session][:token] = @token

      { user: @user, message: @message }
    rescue ActiveRecord::RecordInvalid => e
      GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
    end
  end
end
