module Mutations
  class AddCartItem < BaseMutation
    field :cart_item, Types::CartItemType, null: true
    field :message, String, null: true

    argument :pet_item_id, Integer, required: true

    def resolve(pet_item_id:)
      @message = ''

      if context[:current_user]
        @cart_item = CartItem.create!(cart_id: context[:current_user].customer.cart.id, pet_item_id: pet_item_id,
                                      quantity: 1)
        @customer = Customer.find(context[:current_user].customer.id)
        @message = "#{@customer.user.first_name}'s cart has been created"
        { cart_item: @cart_item, message: @message }
      else
        @message = 'You have to be logged in before you can add any pet items to your cart'
        { cart_item: nil, message: @message }
      end
    end
  end
end
