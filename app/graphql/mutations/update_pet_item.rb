module Mutations
  class UpdatePetItem < BaseMutation
    field :pet_item, Types::PetItemType, null: true
    field :message, String, null: true

    argument :id, ID, required: true
    argument :name, String, required: false
    argument :description, String, required: false
    argument :price, Float, required: false
    argument :inventory, Integer, required: false
    argument :is_available, Boolean, required: false

    def resolve(id:, price: nil, name: nil, description: nil, inventory: nil, is_available: nil)
      @message = ''

      if context[:current_user] && context[:admin?]
        @pet_item = PetItem.find(id)

        if @pet_item
          if name
            @pet_item.name = name
            @pet_item.save
          end
          if description
            @pet_item.description = description
            @pet_item.save
          end
          if price
            @pet_item.price = price
            @pet_item.save
          end
          if inventory
            @pet_item.inventory = inventory
            @pet_item.save
          end
          if is_available
            @pet_item.is_available = is_available
            @pet_item.save
          end
          @message = 'A pet item has been updated'
        else
          @message = 'A pet item cannot be found'
        end
        { pet_item: @pet_item, message: @message }
      else
        @message = 'You have to be logged in as admin before you can create pet items'
        { pet_item: nil, message: @message }
      end
    end
  end
end
