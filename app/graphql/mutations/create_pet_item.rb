module Mutations
  class CreatePetItem < BaseMutation
    field :pet_item, Types::PetItemType, null: true
    field :message, String, null: true

    argument :name, String, required: true
    argument :description, String, required: false
    argument :price, Float, required: true
    argument :inventory, Integer, required: false
    argument :is_available, Boolean, required: false

    def resolve(name:, description:, price:, inventory:, is_available: nil)
      @message = ''

      if context[:current_user] && context[:admin?]
        @pet_item = PetItem.create!(name: name, description: description, price: price, inventory: inventory,
                                    is_available: is_available)
        @message = 'A new pet item has been created'
        { pet_item: @pet_item, message: @message }
      else
        @message = 'You have to be logged in as admin before you can update any pet items'
        { pet_item: nil, message: @message }
      end
    end
  end
end
