module Mutations
  class DeletePetItem < BaseMutation
    field :message, String, null: true

    argument :id, ID, required:

    def resolve(id:)
      @message = ''

      if context[:current_user] && context[:admin?]
        @pet_item = PetItem.find(id)

        if @pet_item
          @pet_item.delete
          @message = 'A pet item has been deleted'
        else
           @message = 'A pet item cannot be found'
        end

        { message: @message }
      else
        @message = 'You have to be logged in as admin before you can create pet items'
        { message: @message }
      end
    end
  end
end
